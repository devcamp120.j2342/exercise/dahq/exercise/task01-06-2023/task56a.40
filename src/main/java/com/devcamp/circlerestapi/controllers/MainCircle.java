package com.devcamp.circlerestapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainCircle {
    @GetMapping("/circle-area")
    public double circleApi(@RequestParam(required = true, name = "string") Double radius) {
        Circle circle1 = new Circle(radius);
        Circle circle2 = new Circle(7.5);
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());
        System.out.println(circle2.getArea());
        System.out.println(circle2.getCircumference());
        return circle1.getArea();

    }

}
